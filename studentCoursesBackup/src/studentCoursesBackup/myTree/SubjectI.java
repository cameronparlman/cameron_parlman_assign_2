package studentCoursesBackup.myTree;
import studentCoursesBackup.myTree.ObserverI;
import studentCoursesBackup.myTree.Node;
/**
SubjectI
*/


public interface SubjectI
{
	

	public void notifyObservers();
	public void registerObserver(Node node_in);
	public void removeObserver(Node node_in);
	
}
