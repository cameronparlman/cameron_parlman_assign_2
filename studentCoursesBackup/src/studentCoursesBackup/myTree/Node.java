package studentCoursesBackup.myTree;

import java.util.ArrayList;
import java.lang.StringBuilder;

/**
Node represents a student
program requires protoype pattern
implements cloneable interface
overides cline();
implements SubjectI  interface 
implements ObserverI interface 
*/
public class Node implements Comparable, Cloneable, ObserverI, SubjectI
{
	private int bnumber;
	private ArrayList<String> courseNames; 	
	private ArrayList<Node> observers;
	private Node left;
	private Node right;
	private Node parent;
	
	/**--GETTERS--*/
	
	/**
		@return the left Node child
	*/
	public Node getLeft(){return left;}


	/**
		@return the right Node child
	*/
	public Node getRight(){return right;}


	/** 
		@return the right Node child	
	*/
	public Node getparent(){return parent;}


	/** 
		@return the list of Nodes observing this one	
	*/
	public ArrayList<Node> getObserversList(){ return observers;}


	/** 
		@param the index of an observer 
		@return An observer at index i	
		@throws ArrayIndexOutOfBoundsException if index > observers length
	*/
	public Node getObserver(int i){ return observers.get(i);}	


	/** 
	@return  the bnumber(identifier) of this node
	*/
	public int getbnumber(){return bnumber;}


	/** 
		@return an ArrayList of courses a student takes 			
	*/
	public ArrayList<String>  getCourseNames(){return courseNames;}	
	


	/**--SETTERS--*/

	/** 
	@param node_in to assign as left child	
	*/
	public void setLeft(Node node_in){left = node_in;}


	/**
	@param node_in to assign as right child	
	*/
	public void setRight(Node node_in){right = node_in;}


	/**
	@param node_in to assign as parent	
	*/
	public void setParent(Node node_in){parent = node_in;}


	/**
	@param bnumber_in to set as bnumber(identifier) of this node
	*/
	public void setbnumber(int bnumber_in){bnumber = bnumber_in;}


	/**
	will notify observers
	@param string_in insert a course name into a list of courses (courseNames)
	*/
	public void insertCourse(String string_in){
		if(!courseNames.contains(string_in)){
			courseNames.add(string_in.toUpperCase());
			notifyObservers();	
		}
	}

	/** 
	will notify observers
	@param string_in remove a course from courseNames 
	*/
	public void removeCourse(String string_in){
		courseNames.remove(string_in);	
		notifyObservers();
	}



	/**--HELPER FUNCTIONS--*/

	/**
	@param course_name check against list of courses taken 
	@return boolean true if courseNames contains the course	
	*/
	public boolean hasCourse(String course_name){return courseNames.contains(course_name);	}


	/**-- OBSERVER PATTERN METHODS--*/

	//observer methods
	/**
	@param bnumber_in set the bnumber(identifier) of the node
	@param courseNames_in replace existing list of courses
	*/
	public void update(int bnumber_in, ArrayList<String> courseNames_in){
		//System.out.println("As an observer i have updated");
		bnumber = bnumber_in;
		courseNames = courseNames_in;	
	}



	/**-- SUBJECT METHODS--*/

	//subject methods 
	/** 
		as part of the observers pattern this will notify / call update for all registered observers 
	*/
	public void notifyObservers(){
		//System.out.println("As a Subject i notify my observers");
		for(Node node_itor : observers){
			//node_itor.update(bnumber, courseNames, observers, left, right, parent);
			node_itor.update(bnumber, courseNames);
		}
	}


	//registerObserver
	/** 
		@param node_in registers a node as an observer of this node 	
	*/
	public void registerObserver(Node node_in){
		//System.out.println("as a Subject i register an observer");
		observers.add(node_in);		
	}
	

	//remove observers
	/** 
	@param node_in is a node to remove from the list of observers 	
	*/
	public void removeObserver(Node node_in){
		//search through obervers. if bnumber == .. obersvers.remove(e));
		//System.out.println("As a Subject i remove an observer");
	}
	

	
	/**-- --*/
	/**
	prints a Node in the form
	bnumber:courseName
	where if a node has multiple classes the node will appear on a new line for each course. 	
	*/
	public String toString(){
		StringBuilder sb = new StringBuilder();	
		if(courseNames.isEmpty()){
			sb.append(bnumber);
			sb.append(":");
			return sb.toString();
		}
		else{
			sb.append(bnumber);
			sb.append(":");
			for(String s : courseNames){
			if(s != "" &&  courseNames.indexOf(s) != courseNames.size()-1 ){
				sb.append(s);	
				sb.append(" ");
			}
			else if(courseNames.indexOf(s) == courseNames.size()-1){
				sb.append(s);
				//sb.append("\n");	
			}
			/**
				if(s != "" &&  courseNames.indexOf(s) != courseNames.size()-1 ){
					sb.append(bnumber);
					sb.append(":");
					sb.append(s);
					sb.append("\n");
				}
				else if(courseNames.indexOf(s) == courseNames.size()-1){
					sb.append(bnumber);
					sb.append(":");
					sb.append(s);
				}
			*/
			}
		}
		return sb.toString();
	}


	//comparable
	/**
	overrides compareTo interface method	
	@param otherObject is the other object that will be compared to this object 
	@return int representing the difference between two Nodes(students) bnumbers
	*/
	public int compareTo(Object otherObject){
		Node other = (Node) otherObject;
		return bnumber - other.getbnumber(); 
	}


	/**clone */
	/** 
		As part of the prototype pattern this returns a clone of this object 	
	*/
	public Object clone()
	{
		try
		{
			Node cloned = (Node) super.clone();	
					
		//	System.out.println("clone may need to clone some other things... plus new pointer things to other node things... ");
			return cloned;	
		}
		catch(CloneNotSupportedException e)
		{
			return null;	
		}
	}		


/**--CONSTRUCTORS --*/

	/**Constructor no Parameters  */
	/** 
		Constructs an empty Node 
			bnumber=-1	
			left = right = parent = null
			courseNames & observers are empty
	*/
	public Node(){
		bnumber=-1;
		left = null;
		right = null;
		parent = null;
		courseNames = new ArrayList<String>();
		observers = new ArrayList<Node>();
	}

	/**CONSTRUCTOR W/ PARAMETERS*/
	/**
	@param bnumber_in assigns bnumber
	@param className_in is the first inserted course of the Node 
	*/
	public Node(int bnumber_in, String className_in){
		left = null;
		right = null;
		parent = null;
		courseNames = new ArrayList<String>();
		observers = new ArrayList<Node>();
		bnumber=bnumber_in;
		courseNames.add(className_in);	
	}
		
	



}

