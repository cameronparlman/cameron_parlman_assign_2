package studentCoursesBackup.util;
import 	studentCoursesBackup.myTree.Node;
import java.lang.StringBuilder;

/** 	
Resourses 
	tree insert from Thomas H Cormen introduction to algorithms 3rd edition  ch13.3 pg  294
	tree binary search from Thomas H Cormen introduction to algorithms 3rd edition  ch13.3 
*/


/**
TreeBuilder a class that builds trees of type studentCoursesBackup.myTree.Node
@see studentCoursesBackup.myTree.Node
*/
public class TreeBuilder{
	
	Node node_orig;		
	Node backup_Node1;
	Node backup_Node2;

	/** 
	default empty constructor 	
	*/
	public TreeBuilder(){
		node_orig = new Node();
	}

	
	/**
	modified tree insert from Thomas H Cormen introduction to algorithms 3rd edition ch13.3 pg  294
	@param root, a Node object that represents the root of a tree 
	@param node_in the node to be inserted into the tree 
	*/
	public void insert(Object root,Object node_in){
		Node y = null;
		Node x = (Node) root;
		while( x != null){
			y = x;
			if(((Node)node_in).compareTo(x) < 0){ x = x.getLeft();}
			else{ x = x.getRight();}
		}		
		((Node)(node_in)).setParent((Node)y);
		if( y == null){ root = (Node)node_in;}
		else if(((Node)node_in).compareTo(y) < 0){	y.setLeft((Node)node_in);}
		else{ y.setRight((Node)node_in);}	
		
	}	
	

	/**
	prints the tree represented by root in-order to command line
	@param root Node object representing the root of a tree 
	*/
	public void printInOrder(Object root){
		if(root != null){
			printInOrder(((Node) root).getLeft());
			System.out.println(((Node)root).toString());
			printInOrder(((Node) root).getRight());
		}
	}


	//print preorder
	/**
	prints the tree represented by root in pre-order to command line
	@param root Node object represents root of a tree
	*/
	public void printPreOrder(Object root){
		if(root != null){
			System.out.println(((Node) root).toString());
			printInOrder(((Node) root).getLeft());
			printInOrder(((Node) root).getRight());
		}	
	}


	//printNodes 
	/**
	writes tree Node data to a Results object 
	@param results_in the Results object to store info in
	@param root_in the root of a tree to write to the Results object 
	*/
	public void printNodes(Results results_in, Object root_in){
		if(root_in != null){
			printNodes(results_in, ((Node)root_in).getLeft());
			results_in.insert(((Node)root_in).toString());
			printNodes(results_in, ((Node)root_in).getRight());
		}
	}
	

	/**
	modified binary search  from Thomas H Cormen introduction to algorithms 3rd edition ch13.3 
	search a tree for a node based on the nodes bnumber(identifier) in binary search tree form 	
	@param root the root of a tree to search 
	@param node_in the node to search for based on its bnumber
	*/
	public Node treeHas(Object root, Object node_in){
		if(root == null || (((Node)node_in).getbnumber() == ((Node)root).getbnumber())){
			return (Node)root;	
		}	
		if( ((Node)node_in).compareTo((Node)root) < 0 ){ 
			return treeHas(((Node)root).getLeft(), node_in);
		}
		else{ 
			return treeHas(((Node)root).getRight(), node_in);
		}	
	}
}
