package studentCoursesBackup.test;

//import static org.junit.Assert.*;
//import org.junit.Before;
//import org.junit.Test;
import studentCoursesBackup.myTree.Node;
import studentCoursesBackup.util.TreeBuilder;
import studentCoursesBackup.util.Results;
import java.util.ArrayList;

public class nodeTest{
	public static void main(String[] args){	
		Results results = new Results();
		Node testNode1 = new Node();
		Node testNode2 = new Node();	
		Node testNode3 = new Node();	
		Node testNode4 = new Node();	
		Node testNode5 = new Node();	
		
		//Test1 setbnumber(int) and getbnumber(int)
		//set some bnumbers
		testNode1.setbnumber(1231);	
		testNode2.setbnumber(1232);	
		testNode3.setbnumber(1233);	
		testNode4.setbnumber(1234);	
		testNode5.setbnumber(1235);	
		
		//test some bnumbers 
		if(testNode1.getbnumber() != 1231|| testNode5.getbnumber() != 1235){
			results.insert("Test1 setbnumber,getbnumber: FAILED\nExpected: testNode1.getbnumber()["+testNode1.getbnumber()+" == 1231");
			results.insert("Test1\tExpected: testNode1.getbnumber()["+testNode5.getbnumber()+" == 1235");
		}else{
			results.insert("Test1 PASSED\ttestNode1.getbnumber() == 1231 && testNode1.getbnumber() == 1235");
		}	

		
		
		//Test2 node.insertCourse(String c), node.hasCourse(String c),
		//insert some course names 
		testNode1.insertCourse("A");
		testNode1.insertCourse("C");		
		testNode2.insertCourse("F");		
		testNode3.insertCourse("D");		
		testNode4.insertCourse("G");		
		testNode5.insertCourse("H");		

			
		//Test2 test nodes have courses
		//2a	
		if(testNode1.hasCourse("A")){
			results.insert("Test2a PASSED\ttestNode1 has course A and testNode1.hasCourse(\"A\") returned True");
		}else{results.insert("Test2a FAILED\ntestNode1.hasCourse(\"A\") Expected True but was False");}
		//2b
		if(testNode1.hasCourse("B")){
			results.insert("Test2b FAILED\ntestNode1.hasCourse(\"B\") Expected false but was true");
		}else{
			results.insert("Test2b PASSED\ttestNode1.hasCourse(\"B\") Expected true");
		}

		

		//Test3 compareTo(Node) TEST < , = , > 
		if(testNode1.compareTo(testNode2) < 0){
			results.insert("Test3a PASSED\ttestNode1.compareTo(testNode2) Expected -1");
		}else{
			results.insert("Test3a FAILED\ntestNode1.compareTo(testNode2) Expected  -1 but was "+ testNode1.compareTo(testNode2));
		}
		if(testNode1.compareTo(testNode1) == 0){
			results.insert("Test3b PASSED\ttestNode1.compareTo(testNode1) Expected 0");
		}else{
			results.insert("Test3b FAILED\ntestNode1.compareTo(testNode1) Expected 0 but was "+ testNode1.compareTo(testNode1)); 
		}
		if(testNode2.compareTo(testNode1) >0){
			results.insert("Test3c PASSED\ttestNode2.compareTo(testNode1) Expected 1");
		}else{
			results.insert("Test3b FAILED\ntestNode2.compareTo(testNode1) Expected > 1 but was "+ testNode2.compareTo(testNode1)); 
		}
		


		//insert nodes into tree// build tree
		//test insert tree tree builder.. 
		Node tree = null;		
		Node backup_tree1 = null;
		Node backup_tree2 = null;
		TreeBuilder treebuilder = new TreeBuilder();

		
		tree = testNode3;
		//clone tree
		backup_tree1 = (Node)tree.clone();
		backup_tree2 = (Node)tree.clone();
		 	
		//arrayLists to hold clones of nodes 
		ArrayList<Node> nodes_orig = new ArrayList();
		ArrayList<Node> clones1 = new ArrayList();
		ArrayList<Node> clones2 = new ArrayList();

		//add nodes to arraylist of nodes original
		nodes_orig.add(testNode1);
		nodes_orig.add(testNode2);
		nodes_orig.add(testNode3);
		nodes_orig.add(testNode4);
		nodes_orig.add(testNode5);


		//clone all nodes 
		//clones 1
		clones1.add((Node)testNode1.clone());
		clones1.add((Node)testNode2.clone());
		clones1.add((Node)testNode3.clone());
		clones1.add((Node)testNode4.clone());
		clones1.add((Node)testNode5.clone());
		//clones 2
		clones2.add((Node)testNode1.clone());
		clones2.add((Node)testNode2.clone());
		clones2.add((Node)testNode3.clone());
		clones2.add((Node)testNode4.clone());
		clones2.add((Node)testNode5.clone());
	
		//treebuilder.insert(tree, testNode5);
		treebuilder.insert(tree, testNode1);
		treebuilder.insert(tree, testNode4);
		treebuilder.insert(tree, testNode5);
		treebuilder.insert(tree, testNode2);
		//insert clones into trees
		treebuilder.insert(backup_tree1, clones1.get(0));
		treebuilder.insert(backup_tree1, clones1.get(3));
		treebuilder.insert(backup_tree1, clones1.get(4));
		treebuilder.insert(backup_tree1, clones1.get(1));
		
		treebuilder.insert(backup_tree2, clones2.get(0));
		treebuilder.insert(backup_tree2, clones2.get(3));
		treebuilder.insert(backup_tree2, clones2.get(4));
		treebuilder.insert(backup_tree2, clones2.get(1));

		//register observers 
		testNode1.registerObserver(clones1.get(0));
		testNode1.registerObserver(clones2.get(0));
		testNode2.registerObserver(clones1.get(1));
		testNode2.registerObserver(clones2.get(1));
		testNode3.registerObserver(backup_tree1);
		testNode3.registerObserver(backup_tree2);
		testNode4.registerObserver(clones1.get(3));
		testNode4.registerObserver(clones2.get(3));
		testNode5.registerObserver(clones1.get(4));
		testNode5.registerObserver(clones2.get(4));

		
		//modifying values of Nodes
		testNode1.insertCourse("K");
		testNode1.insertCourse("I");
		testNode2.insertCourse("I");
		testNode3.insertCourse("B");
	
		//Test4 
		if(testNode1.getObserver(0).hasCourse("K")){
			results.insert("Test4 PASSED\ttestNodes observer Expected to have course K");
		}else{
			results.insert("Test4 FAILED\ntestNodes observer Expected to have course K");
		}

		
		//Test5
		testNode1.removeCourse("K");
		//Test5a
		if(testNode1.hasCourse("K")){
			results.insert("Test5a FAILED\ntestNode1 should not have course K remove successfull");
		}else{
			results.insert("Test5a PASSED\ttestNode1 does not have course K after removeCourse(K)");	
		}
		//Test5b 
		if(testNode1.getObserver(0).hasCourse("K")){
			results.insert("Test5b FAILED\ntestNode1s observer should not have course K remove and notify successfull");
		}else{
			results.insert("Test5b PASSEED\tobserver of testNode1 should not have course K");
		}
	
		//Test6
		//Test6a
		if(testNode2.hasCourse("I")){
			results.insert("Test6a PASSED\ttestNode2 has course I");	
		}else{
			results.insert("Test6a FAILED\ntestNode2 should have course I");
		}
		//Test5b 
		if(testNode2.getObserver(0).hasCourse("I")){
			results.insert("Test6b PASSEED\tobserver of testNode2 has course I(notifyobservers) successfull");
		}else{
			results.insert("Test6b FAILED\ntestNode1s observer should have course I");
		}
	
			

		
		
		

		//write results
		results.writeToStdout("");

	}
}

