package studentCoursesBackup.driver;

import studentCoursesBackup.util.FileProcessor;
import studentCoursesBackup.util.TreeBuilder;
import studentCoursesBackup.myTree.Node;
import studentCoursesBackup.util.Results;
import java.util.ArrayList;

/**
Runs the studentCoursesBackup program
@author cameron parlman
@author cparlma1@binghamton.edu
@version 1.0
@since 0.0
*/
public class Driver{
	public static void main(String[] args){
		Node studentCoursesRoot = null;
		TreeBuilder treebuilder = new TreeBuilder();


		//comand line verification
		if(args.length != 5){usage();return ;}
		else{
		//	System.out.println("0:"+args[0]+ "\t1:"+args[1]+ "\t2:"+args[2]+"\t3:"+args[3]+"\t4:"+args[4]);	
		}		
		
		//I/O file names 
		String file_input_name = args[0];
		String file_delete_name = args[1];	
		String file_output1_name = args[2];
		String file_output2_name = args[3];
		String file_output3_name = args[4];


		FileProcessor fileprocessor = new FileProcessor(file_input_name);
		//read the input file input file with fileprocessor
		String line = "";	
		while(fileprocessor.hasNext()){
			line = fileprocessor.readLine();
				
			//parse the previously read line
			String delims = ":";	
			String[] tokens = line.split(delims);	
			
			//assign bnumber from parsed line
			int bnumber = Integer.parseInt(tokens[0]);	
			
			//Create a Node named buildNode and clone it twice 
			Node buildNode = new Node(bnumber , tokens[1]);				
			Node clone1 = (Node) buildNode.clone();
			Node clone2 = (Node)buildNode.clone();
	
			//Insert buildNode into tree or insert a course name and register 
			if(studentCoursesRoot == null){//if the root is null, Root = buildNode 
				studentCoursesRoot = buildNode;
				studentCoursesRoot.registerObserver(clone1);	
				studentCoursesRoot.registerObserver(clone2);	
				
			}
			//if student does not exist exist int the tree, create it, register its clones as observers, insert Node into each tree
			else if((buildNode = treebuilder.treeHas(studentCoursesRoot, buildNode))== null){ //if student found in tree, return to buildNode 
				buildNode = new Node(bnumber, tokens[1]);
				buildNode.registerObserver(clone1);
				buildNode.registerObserver(clone2);
				treebuilder.insert(studentCoursesRoot, buildNode);
				treebuilder.insert(studentCoursesRoot.getObserver(0), clone1);
				treebuilder.insert(studentCoursesRoot.getObserver(1), clone2);
			}
			else //student exists already, insert course, notify observers 
			{
					buildNode.insertCourse(tokens[1]);
			}
		}/** End read input file */
	


		//OPEN FILE FOR DELETE and read by line, tokenize, build temp Node, check if temp is in tree, if so remove the course
		FileProcessor delete_file_processor = new FileProcessor(file_delete_name);
		while(delete_file_processor.hasNext()){
			line = delete_file_processor.readLine();
			String delims = ":";
			String[] tokens = line.split(delims);
			//for(String tok : tokens){System.out.println("\n("+tok+")");}
			int bnumber = Integer.parseInt(tokens[0]);	
			Node buildNode = new Node (bnumber, tokens[1]);
			Node tmp = treebuilder.treeHas(studentCoursesRoot, 	buildNode);
				try{
				tmp.removeCourse(tokens[1]);	
				}catch(NullPointerException e){System.out.println("NullPointerException Caught");return;}
		}


		//STORE RESULTS 
		Results results1= new Results();
		Results results2= new Results();
		Results results3= new Results();
		
		//print nodes to results 
		treebuilder.printNodes(results1, studentCoursesRoot);
		treebuilder.printNodes(results2, studentCoursesRoot.getObserver(0));
		treebuilder.printNodes(results3, studentCoursesRoot.getObserver(1));
					
		//write results to file 
		results1.writeToFile(file_output1_name);
		results2.writeToFile(file_output2_name);
		results3.writeToFile(file_output3_name);

	}


	/**
	prints the usage string of studentCoursesBackup
	*/
	public static void usage(){
		System.out.println("studentCoursesBackup inputfile deletefile output1 output2 output3");
	}
}
