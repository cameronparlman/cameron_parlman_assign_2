Assuming you are in the same directory as the this README

## To Clean
ant -buildfile studentCoursesBackup/src/build.xml clean

-----------------------------------------------------------------------------------------
## To Compile
ant -buildfile studentCoursesBackup/src/build.xml all


-----------------------------------------------------------------------------------------
## To Run by specifying arguments from command line 
	#COPY AND Past(may have break in copied text where new line is in editor) mine is vim and breaks "output1.txt" into "output1.t xt"
	run where args can be specified relative to the build.xml
ant -buildfile studentCoursesBackup/src/build.xml run -Darg0=input_file/inputBig.txt -Darg1=input_file/deleteBig.txt -Darg2=output_file/output1.txt -Darg3=output_file/output2.txt -Darg4=output_file/output3.txt


-----------------------------------------------------------------------------------------
## To Run by specifying arguments from command line, where input files are from /home/usr/      //requires actual user in path 
	#!REPLACE cameron with your user name. sorry.. 
	run
ant -buildfile studentCoursesBackup/src/build.xml run -Darg0=/home/cameron/input_file/input.txt -Darg1=/home/cameron/input_file/delete.txt -Darg2=/home/cameron/output_file/output1.txt -Darg3=/home/cameron/output_file/output2.txt -Darg4=/home/cameron/output_file/output3.txt


-----------------------------------------------------------------------------------------
## To Run default args, buildfile has default args specified to studentCoursesBackup/input_file/ and sutdentCoursesBackup/output_file/
ant -buildfile studentCoursesBackup/src/build.xml run


-----------------------------------------------------------------------------------------
## To Test
ant -buildfile studentCoursesBackup/src/build.xml test


-----------------------------------------------------------------------------------------
## To Create TarBall for submission 
//unless tar action specified by build.xml
first clean, 
from parent directory run
tar -cvf cameron_parlman_assign_2.tar cameron_parlman_assign_2


-----------------------------------------------------------------------------------------
## Create javadocs
run
javadoc -d studentCoursesBackup/src/studentCoursesBackup/docs/ -sourcepath studentCoursesBackup/src/ studentCoursesBackup.myTree studentCoursesBackup.util



-----------------------------------------------------------------------------------------

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense.â€

[Date: 9/3/2017] -- Please add the date here
Cameron Parlman.

-----------------------------------------------------------------------
##Data Structures and complexity in terms of Big O //time and space 
Binary search tree. made of Nodes which represent students 
	bnumbers are the key / what is compared between two Nodes 
The Binary Search Tree has a best run time of O(log n) on relatively normal datasets 
	However if the data is already sorted time complexity goes to worst case O(n)
	
ArrayLists used in storing class names. 
finding a class name O(n)

-----------------------------------------------------------------------------------------
##Citations
Binary Tree insert from Thomas H Cormen introduction to algorithms 3rd edition  ch13.3 pg  294
Binary Tree search from Thomas H Cormen introduction to algorithms 3rd edition  ch13.3 


-----------------------------------------------------------------------
##Notes  while developing 
Copy and pasting from vim can cause unexpected breaks in the copied text 
resorted to using insert on cloned list. dont know if serialize would work .. 

